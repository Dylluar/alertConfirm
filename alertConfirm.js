//Mensagem de confirmação usando o plugin SweetAlert para links ou formulários
//http://t4t5.github.io/sweetalert/
//busca as diretivas data-title e data-text para gerar as mensagens
//opcionalmente, data-text-cancel, data-btn-cancel e data-btn-confirm 

(function () {
    $('.alert-confirm').on('click', function (ev) {
        ev.preventDefault();

        var title = $(this).data('title');
            title = title ? title : "";
            
        var text = $(this).data('text');
        
        var textCancel = $(this).data('text-cancel');
            textCancel = textCancel ? textCancel : "Cancelado";
            
        var form = $(this).closest("form");
        var route = $(this).attr('href');

        var btnConfirm = $(this).data('btn-confirm');
            btnConfirm = btnConfirm ? btnConfirm : "Sim";

        var btnCancel = $(this).data('btn-cancel');
            btnCancel = btnCancel ? btnCancel : "CANCELAR";

        swal({
            title: title,
            text: text,
            type: "warning",
            html: true,
            showCancelButton: true,
            confirmButtonText: btnConfirm,
            cancelButtonText: btnCancel,
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                if (form) form.submit();
                if (route) window.location.href = route;
            } else {
                swal(textCancel, "", "error");
            }
        });
    });
})();
